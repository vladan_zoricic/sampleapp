Rails.application.routes.draw do
	root to: "categories#index"

  resources :articles
  resources :categories
end